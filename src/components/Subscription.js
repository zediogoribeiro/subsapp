import React from 'react';

const Subscription = ({ subscription }) => {

    const price = () => {
        return subscription.price > 0 ? <div> {subscription.price}<sup>{'\u20AC'}</sup></div> : 'Free';
    }

    return (
        <ul>
            <li>
                <span>{subscription.name}</span>
                <h3>
                    { 
                      price()
                    }
                </h3>
            </li>
        </ul>
    )
    
}

export default Subscription;
