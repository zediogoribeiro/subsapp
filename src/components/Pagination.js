import React from 'react';
import '../style/Pagination.css';
import '../style/shared.css';


const Pagination = ({ applicationsPerPage, totalApps, currentPage, paginate }) => {
    const pageNumbers = [];
    

    for(let i=1; i <= Math.ceil( totalApps / applicationsPerPage); i++) {
      pageNumbers.push(i);
    }
    

    const canChangePage = (page) => {
        if(page > 0 && page <= pageNumbers.length)
            paginate(page);
    }

    return (
        <ul className="pagination">
            <li key='previus'>
                <a onClick={() => canChangePage(currentPage - 1)} href="!#">{'<'}</a>
            </li>
            {
                pageNumbers.map(number => (
                    <li 
                        disabled
                        key={number} 
                        className={'page-item' + (currentPage === number ? ' active' : '')} 
                    >
                        <a onClick={() => paginate(number)} href='!#' className="page-link">
                            {number}
                        </a>
                    </li>
                ))
            }
            <li key='next'>
                <a onClick={() => canChangePage(currentPage + 1)} href='!#'>{'>'}</a>
            </li>
        </ul>
    )
}

export default Pagination;