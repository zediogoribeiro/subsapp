import React from 'react';
import Application from './Application';

const Applications = ({ currentApps }) => {
    return (
        <ul>
            {
                currentApps.map((application) => (
                    <Application key={application.id} application={application} />
                ))
            }
        </ul>
    );
}

export default Applications;
