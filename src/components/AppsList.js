import React, { useEffect, useState } from 'react';
import { sortApps, sortCategories} from '../Utils';
import Applications from './Applications';
import Pagination from './Pagination';
import SearchBar from './SearchBar';
import NavMenu from './NavMenu';
import '../style/shared.css';
import '../style/AppsList.css';


function AppList() {
    const [applications, setApplications] = useState([]);
    const [filteredApps, setFilteredApps] = useState([]);
    const [currentApps, setCurrentApps] = useState([]);
    const [categories, setCategories] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [filter, setFilter] = useState('');

    let applicationsPerPage = 3;

    const getCurrentApps = (apps) => {
        const indexOfLastApp = currentPage * applicationsPerPage;
        const indexOfFristApp = indexOfLastApp - applicationsPerPage;
        setCurrentApps(apps.slice(indexOfFristApp, indexOfLastApp));
    }
    
    const handlerCategorieClick = (categorie) => {
        setFilter(categorie);
    }

    const handlerPaginate = (pageNumber) => {
        setCurrentPage(pageNumber);
    };

    useEffect(() => {
        if(filter.length !== 0) {
            setCurrentPage(1);

            let filterxx = applications.filter((app) => (
                app.categories.some(categorie => categorie.toLowerCase().includes(filter.toLowerCase()))
            ))
            setFilteredApps(filterxx); 

            getCurrentApps(filterxx);
        } else {
            getCurrentApps(applications);
        }
        
    }, [filter]);

    useEffect(() => {
        if(filter.length !== 0) {
            getCurrentApps(filteredApps);
        } else {
            getCurrentApps(applications);
        }
    }, [currentPage]);

    useEffect( () => {
        const getData = () => {
            fetch('apps.json', 
            {
                headers: {
                    'Content-type': 'application/json',
                    'Accept': 'application/json'
                }
            }).then((response) => {
                return  response.json();
            }).then((applications) => {
                setApplications(sortApps(applications));
                setCategories(sortCategories(applications));
                getCurrentApps(applications);
            })
        }

        getData();
    }, []);

    return (
        <div className="flex-container">
            <NavMenu 
                categories={categories}
                filter={filter}
                handlerCategorieClick={handlerCategorieClick}
            />
            <section className="apps-list">
                <SearchBar filter={filter} hanlderFilter={setFilter} />
                <Applications currentApps={currentApps} />
                <Pagination
                    applicationsPerPage={applicationsPerPage}
                    totalApps={filter.length !== 0 ? filteredApps.length : applications.length} 
                    currentPage={currentPage}
                    paginate={handlerPaginate}   
                />
            </section>   
        </div>
    )
}

export default AppList;