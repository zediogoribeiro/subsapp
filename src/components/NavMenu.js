import React from 'react';
import '../style/shared.css';
import '../style/NavMenu.css';


const NavMenu = ({categories, filter , handlerCategorieClick}) => {
    return (
        <nav className="nav-categories">
            <h2>Categorias</h2>
            <ul className="nav-menu">
            {
                categories.map((categorie) => (
                   <li key={categorie} className={categorie === filter ? 'active' : ''}>
                        <a onClick={() => handlerCategorieClick(categorie)} href="!#">{categorie}</a>
                    </li>
                ))

            }
            </ul>
        </nav>
    );
}

export default NavMenu;