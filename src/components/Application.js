import React from 'react';
import '../style/Application.css';
import '../style/shared.css';
import Subscription from './Subscription';


const Application = ({ application }) => {
    return (
        <li>
            <div className="app-item">
                <div className="box-info">
                    <div className="box-info--content">
                        <div className="description">
                            <h1>{application.name}</h1>
                            <p>{application.description}</p>
                        </div>
                        <div className="tags">
                            {
                                application.categories.map((category, index) => (
                                    <span key={index}> {category} </span> 
                                ))
                            }
                        </div>
                    </div>
                    <div className="box-info--footer">
                        {application.subscriptions.map((subscription, key) => (
                            <Subscription key={key} subscription={subscription} />
                        ))}
                    </div>
                </div>
            </div>
        </li>
    )
}

export { Application as default }