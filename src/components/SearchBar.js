import React from 'react';

const SearchBar = ({ hanlderFilter, filter }) => {
    return (
        <header>
            <input type="text" placeholder="Procura por Categoria" onChange={e => hanlderFilter(e.target.value)} value={filter}></input>
        </header>
    );
}

export default SearchBar;
