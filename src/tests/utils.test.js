import { sortApps, sortCategories} from '../Utils';
import orderedJson from './fixtures/orderedJson';
import unorderJson from './fixtures/unorderJson.js';
import unorderCategories from './fixtures/unorderCategories';
import orderCategories from './fixtures/orderCategories';

test('empty Apps', () => {
    expect(sortApps()).toEqual([]);
});

test('except apps to be ordered', () => {
    expect(sortApps(unorderJson)).toEqual(orderedJson);
}); 


test('emptyCategories', () => {
    expect(sortCategories([])).toEqual([]);
});


test('expect categories to be order', () => {
    expect(sortCategories(orderedJson)).toEqual(orderCategories);
});
