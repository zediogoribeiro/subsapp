export default {
    "id":"7f89f001-9d7d-52f1-82cb-8f44eb1e4680",
    "name":"Disney Plus",
    "description":" A casa da Disney, Marvel, Pixar, Star.",
    "categories":[
       "Filmes",
       "Séries"
    ],
    "subscriptions":[
       {
          "name":"Standard",
          "price":6.99
       }
    ]
}