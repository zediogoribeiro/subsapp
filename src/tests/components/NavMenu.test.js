import React from 'react';
import NavMenu from '../../components/NavMenu';
import current from '../fixtures/orderCategories';
import { create } from "react-test-renderer";

const render = create(<NavMenu categories={current} filter={""}/>);

test('should render NavMenu', () => {
    expect(render.toJSON()).toMatchSnapshot();
});