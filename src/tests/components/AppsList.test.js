import React from 'react';
import AppList from '../../components/AppsList';
import { create } from "react-test-renderer";

const render = create(<AppList />);

test('should render the  App', () => {
    expect(render.toJSON()).toMatchSnapshot();
});