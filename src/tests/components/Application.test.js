import React from 'react';
import Application from '../../components/Application';
import app from '../fixtures/singleApplication'
import { create } from "react-test-renderer";

const render = create(<Application application={app}/>);

test('should render one single Application', () => {
    expect(render.toJSON()).toMatchSnapshot();
});