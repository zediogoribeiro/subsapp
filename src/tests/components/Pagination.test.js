import React from 'react';
import Pagination from '../../components/Pagination';
import { create } from "react-test-renderer";

const render = create(<Pagination applicationsPerPage={3} totalApps={12} currentPage={1}/>);

test('should render Pagination', () => {
    expect(render.toJSON()).toMatchSnapshot();
});