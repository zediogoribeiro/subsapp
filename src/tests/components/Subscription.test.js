import React from 'react';
import Subscription from '../../components/Subscription';
import sub from '../fixtures/subscription';
import { create } from "react-test-renderer";

const render = create(<Subscription subscription={sub}/>);

test('should render Subscription', () => {
    expect(render.toJSON()).toMatchSnapshot();
});