import React from 'react';
import Applications from '../../components/Applications';
import current from '../fixtures/unorderJson';
import { create } from "react-test-renderer";

const render = create(<Applications currentApps={current}/>);

test('should render all Applications', () => {
    expect(render.toJSON()).toMatchSnapshot();
});