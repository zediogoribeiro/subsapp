import React from 'react';
import SearchBar from '../../components/SearchBar';
import { create } from "react-test-renderer";

const render = create(<SearchBar filter={'TV'}/>);

test('should render SearchBar', () => {
    expect(render.toJSON()).toMatchSnapshot();
});