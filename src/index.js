import React from 'react';
import ReactDOM from 'react-dom';
import AppList from './components/AppsList';


ReactDOM.render(
  <React.StrictMode>
    <AppList />
  </React.StrictMode>,
  document.getElementById('root')
);


