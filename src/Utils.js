//Sort Applications by price
export const sortApps = (apps = []) => {
    return apps.sort((a,b) => {
       let totalA = 0
       let totalB = 0;

       a.subscriptions.forEach(a => { totalA = totalA + a.price });
       b.subscriptions.forEach(b => { totalB = totalB + b.price });

       if(totalA > totalB) {
           return 1;
       } else {
           return -1;
       }}
   )
};

//Sort Categories by alphabetic order 
export const sortCategories = (applications = []) => {
    let categories = [];

    applications.forEach((application) => {
        application.categories.forEach((categorie) => {   
            categories.push(categorie);
        });
    });

    categories = [...new Set(categories)];

    return categories.sort((a,b) => {
        return a.localeCompare(b);
    });

}